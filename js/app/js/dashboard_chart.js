//

$(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */
    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var storeid= Cookies.get('storeid');
    var productPieData= [];
    var PieData = [
        {
            value: 0,
            color: "#f56954",
            highlight: "#f56954",
            label: "Pending for live"
        },
        {
            value: 0,
            color: "#00a65a",
            highlight: "#00a65a",
            label: "All products"
        },
        {
            value: 0,
            color: "#f39c12",
            highlight: "#f39c12",
            label: "Live products"
        },
        {
            value: 0,
            color: "#00c0ef",
            highlight: "#00c0ef",
            label: "Marked for review"
        },
        {
            value: 0,
            color: "#3c8dbc",
            highlight: "#3c8dbc",
            label: "Discounted products"
        }

    ];

    var orderPieData = [
        {
            value: 0,
            color: "#f56954",
            highlight: "#f56954",
            label: "Cancelled"
        },
        {
            value: 0,
            color: "#00a65a",
            highlight: "#00a65a",
            label: "Pending"
        }

    ];
    var request = $.ajax( {
        url: "http://35.154.54.234:1337/"+"dashboard",
        method: "GET",
        data: "store_id="+storeid,
        dataType: "json"
    });

    request.done(function( response ) {
        //debugger;
        console.log(response);
        localStorage.setItem('data',JSON.stringify(response.data));
        if(response.code == 200){
            var i =0;
            // debugger;
            for(var k in response.data[0].section_data){
                PieData[i].value = response.data[0].section_data[k];
                i++;
            }

            var j = 0;

            for(var k in response.data[1].section_data){
                if(k!='Delivered'){
                    orderPieData[j].value = response.data[1].section_data[k];
                    j++;
                }
            }
            createProductChart(PieData);
            createOrderChart(orderPieData);
        }
        else{

        }
    });

    request.fail(function( jqXHR, textStatus) {

    });

    function createProductChart(GetPiedata) {
        var pieChartCanvas = $("#productChart").get(0).getContext("2d");
        var pieChart = new Chart(pieChartCanvas);

        var pieOptions = {
            //Boolean - Whether we should show a stroke on each segment
            segmentShowStroke: true,
            //String - The colour of each segment stroke
            segmentStrokeColor: "#fff",
            //Number - The width of each segment stroke
            segmentStrokeWidth: 2,
            //Number - The percentage of the chart that we cut out of the middle
            percentageInnerCutout: 50, // This is 0 for Pie charts
            //Number - Amount of animation steps
            animationSteps: 100,
            //String - Animation easing effect
            animationEasing: "easeOutBounce",
            //Boolean - Whether we animate the rotation of the Doughnut
            animateRotate: true,
            //Boolean - Whether we animate scaling the Doughnut from the centre
            animateScale: false,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true,
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
        };
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        pieChart.Doughnut(GetPiedata, pieOptions);
    }


    // order chart


    function createOrderChart(GetPiedata) {
        var pieChartCanvas = $("#orderChart").get(0).getContext("2d");
        var pieChart = new Chart(pieChartCanvas);

        var pieOptions = {
            //Boolean - Whether we should show a stroke on each segment
            segmentShowStroke: true,
            //String - The colour of each segment stroke
            segmentStrokeColor: "#fff",
            //Number - The width of each segment stroke
            segmentStrokeWidth: 2,
            //Number - The percentage of the chart that we cut out of the middle
            percentageInnerCutout: 50, // This is 0 for Pie charts
            //Number - Amount of animation steps
            animationSteps: 100,
            //String - Animation easing effect
            animationEasing: "easeOutBounce",
            //Boolean - Whether we animate the rotation of the Doughnut
            animateRotate: true,
            //Boolean - Whether we animate scaling the Doughnut from the centre
            animateScale: false,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true,
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
        };
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        pieChart.Doughnut(GetPiedata, pieOptions);
    }


});



