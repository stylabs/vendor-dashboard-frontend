/**
 * Created by prasad on 12/06/17.
 */

var product = {
    init: function () {
        this.cacheElement();
        this.addEvent();
        $(document).ajaxStart(function() { Pace.restart(); });

    },

    cacheElement: function () {
        this.productTab = document.querySelectorAll('.product');
    },

    addEvent: function () {

        function attachEvent(element) {
            for (var i = 0; i < element.length; i++) {
                element[i].addEventListener('click', function (e) {
                    var sourceTarget = e.srcElement;
                    var sourceAttr = sourceTarget.dataset.url;

                    var pageTitle = document.querySelector('.page-title');
                        console.log(sourceTarget.text);
                        pageTitle.innerText = sourceTarget.text+' Products';

                        console.log(sourceAttr);
                    if (sourceAttr != '') {
                        var url = "http://35.154.54.234:1337/"+sourceAttr+'';
                        ajaxcall(url, {store_id:Cookies.get('storeid'),page_no:1},'GET',function(response){
                            console.log();
                            $('#contentWrapper').html(prodTemplate.compileTemplate(response.data));
                        },function(error){
                            console.log(error);
                        },function(){
                            console.log('always');
                        });

                    }
                    else {
                        console.log('no data found');
                    }

                });
            }
        }

        attachEvent(this.productTab);

        $('#contentWrapper').on('click','.openVariants',this.openProductVariants.bind(this));
    },

    openProductVariants:function(e){
        var variantaArray = $(e.currentTarget).data('variants');
        var compiledTemplate = VariantTemplate.compileTemplate(variantaArray);
            $('#variantModal').find('tbody').html(compiledTemplate);
            $('#variantModal').modal('show');
    }

}
product.init();

// get all orders

var orders = {
    init:function(){
        this.cacheElement();
        this.addEvent();
    },

    cacheElement : function(){
        this.allOrders = document.querySelector('.orders');
    },

    addEvent: function(){
        this.allOrders.addEventListener('click' , function(e) {
            var sourceTarget = e.srcElement.dataset.url;

            var pageTitle = document.querySelector('.page-title').innerText='Get All Orders';

            if(sourceTarget != ''){
                var apisource = "http://35.154.54.234:1337/"+sourceTarget+'';
                     ajaxcall(apisource ,{store_id:Cookies.get('storeid'),page_no:1},'post', function(response){
                         console.log(response);
                         var orderdata = getallOrderTemplate.compliedTemplate(response.data);
                         $('#ordersTable').find('tbody').html(orderdata);
                         $('.orders-table').DataTable();
                         $('.box-body').css('display' ,'block');

                     },function(error){console.log(error)}, function(){console.log('Always')},
                );
            }
        });

       $('#contentWrapper').on('click','.order-status-edit',this.openOrderStatus.bind(this));
       $('#contentWrapper').on('click','.order-details-btn',this.operOrderDetails.bind(this));

    },

    openOrderStatus:function(e){
        var ordersArray = $(e.currentTarget).data('id');
        var apisource = "http://35.154.54.234:1337/"+'getorderstatuses'+'';

            ajaxcall(apisource,{order_id:ordersArray},'GET',function(response){
               // console.log(response);
                var compiledTemplate = orderStatusTemplate.compileTemplate(response.data);
                $('#orderStatus').find('#options').html(compiledTemplate);
                $('#orderStatus').modal('show');
                var status_id = $(".status-name").val();
                console.log(e);
                console.log(status_id);
                $('.order-status-drpdwn').on('change' , function(e){
                    var status_id = e.currentTarget.value;
                    ajaxcall('http://35.154.54.234:1337/update_order_status',{order_id:ordersArray,status_id:status_id},'POST', function(response){
                        $('#orderStatus').modal('hide');
                        console.log(response);
                    },function(error){
                        console.log(error)
                    },function() {
                        console.log('always');
                    });
                });
                console.log(response.data);
            },function(error){
                console.log(error)
            },function() {
                console.log('always');
            });
    },

    operOrderDetails:function(e){

        var detailsArray = $(e.currentTarget).data('id');
        console.log(detailsArray);
        var apisource = "http://35.154.54.234:1337/"+'getfullorder'+'';
            ajaxcall(apisource,{order_id:detailsArray,store_id:Cookies.get('storeid')},'POST',function(success) {

                var compiledTemplate = orderDetailsTemplate.compileTemplate(success.data);
                $('#orderDetails').find('.order-details').html(compiledTemplate);
                $('#orderDetails').modal('show');
                console.log(success);
            },
                function(error){
                    console.log(error);
                },
                function(){
                    console.log('Always');
                }
            )
    }
}

orders.init();


// ajax common function

ajaxcall = function (url, data, type, onDataFetch, onFetchFail, onAlways ) {
    $.ajax({
        type: type,
        url: url,
        data: data,
        dataType: 'json',
    }).done(function (response) {
        onDataFetch.call(null,response);
    }).fail(function (error) {
        onFetchFail.call(error);
    }).always(function () {
        onAlways.call(null);
    });
}

// product Template

var prodTemplate = {
    wrapper: '<div class="row">{{compliedTemplate}}</div>',
    prodCard:'<div class="col-md-3 col-sm-4 productCard">'
             +   '<div class="img-block">'
             +      '<div class="cardImg_hideOverflow">'
             +       '<a href="#">'
             +          '<img src="{{imageUrl}}" class="cardImg"/>'
             +       '</a>'
             +       '</div>'
             +   '</div>'
             +   '<div class="productCard_info">'
             +       '<button class="btn btn-sm btn-primary openVariants" data-variants=\'{{variants}}\' data-toggle="modal" >'
             +           'Variants'
             +       '</button>'
             +       '<p class="brand-name">{{prodName}}<br></p>'
             +    '</div>'
             +'</div>',


    compileTemplate:function(products){
        var tempWrapper = this.wrapper;
        var mainProdCards = '';
        //debugger;
        console.log(products);
            if(products.length <= 0){
                return 'No Products Found';
            }

        for(var i = 0; i < products.length; i++){
            var product = products[i];
            var prodCard = this.prodCard;
            prodCard = prodCard.replace('{{imageUrl}}', product.product_image[0]);
            prodCard = prodCard.replace('{{prodName}}', product.product_details[1].name);
            prodCard = prodCard.replace('{{variants}}', JSON.stringify(product.variants));
            mainProdCards += prodCard;
        }
        tempWrapper = tempWrapper.replace('{{compliedTemplate}}', mainProdCards);
        return tempWrapper;
    },
}

var VariantTemplate = {
        wrapper :' <tr>{{compliedTemplate}}</tr>',
        variantData:'<td>{{title}}</td>'
                    +'<td>{{ogPrice}}</td>'
                    +'<td>{{price}}</td>'
                    +'<td>{{size}}</td>',

    compileTemplate:function(variants){

         var mainVariant = '';

         for(var i = 0; i < variants.length; i++){
             var variant = variants[i];
             var variantData = this.variantData;
             var temwrapper = this.wrapper;

             //debugger;
             variantData = variantData.replace('{{title}}', variant.title);
             variantData = variantData.replace('{{ogPrice}}', variant.original_price);
             variantData = variantData.replace('{{price}}', variant.price);
             variantData = variantData.replace('{{size}}', variant.size);
             temwrapper = temwrapper.replace('{{compliedTemplate}}', variantData);
             mainVariant += temwrapper
         }

        return mainVariant;
    }
}

// order template
var getallOrderTemplate = {
     wrapper :'<tr>{{compliedTemplate}}</tr>',
    ordersData :'<td>{{orderId}}</td>'
                +'<td>{{storeName}}</td>'
                +'<td>{{storeOrderId}}</td>'
                +'<td>{{dateAdded}}</td>'
                +'<td>{{cusFirstName}}</td>'
                //+'<td>{{payType}}</td>'
                +'<td>{{orderStatus}}<button class="btn btn-default order-status-edit" data-id="{{orderId}}"  data-toggle="modal"><span class="order-status-edit-icon"><i class="fa fa-pencil"></i></span></button></td>'
                +'<td>{{amount}}</td>'
                +'<td>{{settleAmt}}</td>'
                +'<td>{{details}}<button data-id="{{orderId}}" class="btn btn-primary order-details-btn">Details</button></td>',

    compliedTemplate:function(orders){
         var allOrders ='';

         for(var i =0; i < orders.length; i++){
             var order = orders[i];
             var ordersData = this.ordersData;
             var orderWrapper = this.wrapper;

             var m = moment(order.date_added);
             var date = m.utc().format('DD/MM/YYYY');
             var time = m.utc().format('HH:mm');
             ordersData = ordersData.replace(/{{orderId}}/g, order.order_id);

            apisourceSettlement = "http://35.154.54.234:1337/getsettlement";

            var settlement_amount = function () {
                var tmp = null;
                $.ajax({
                    'async': false,
                    'type': "POST",
                    'global': false,
                    'url': apisourceSettlement,
                    'data': {order_id:order.order_id,page_no:1},
                    'success': function (data) {
                        tmp = data.data.settlementAmount;
                    }
                });
                return tmp;
            }();

             ordersData = ordersData.replace('{{settleAmt}}',settlement_amount);

             ordersData = ordersData.replace('{{storeName}}', order.store_name);
             ordersData = ordersData.replace('{{storeOrderId}}', order.store_order_id);
             ordersData = ordersData.replace('{{dateAdded}}', (date+' <br/> '+time));
             ordersData = ordersData.replace('{{cusFirstName}}', order.name);
            // ordersData = ordersData.replace('{{payType}}', order);
             ordersData = ordersData.replace('{{orderStatus}}', order.status_name);
            // ordersData = ordersData.replace('{{trackingID}}', order);
            // ordersData = ordersData.replace('{{courierType}}', order);
             ordersData = ordersData.replace('{{amount}}', order.price);
             //ordersData = ordersData.replace('{{settleAmt}}', order.total);
             ordersData = ordersData.replace('{{details}}', order.model);

             orderWrapper = orderWrapper.replace('{{compliedTemplate}}' , ordersData);
             allOrders += orderWrapper
         }

         return allOrders;
    }
}


var orderStatusTemplate = {
        wrapper :'<select class="btn btn-primary order-status-drpdwn" ><option>Select option</option>{{compliedTemplate}}</select>',
        orderData : '<option value="{{value}}">{{orderstatus}}</option>',

        compileTemplate:function(orderStatus) {
            var allorderStatus = '';
            var orderwrapper = this.wrapper;
           // console.log(orderStatus.order_status);

            for(var i =0; i < orderStatus.order_status.length; i++){

                var allorderstatus = orderStatus.order_status[i];
                var orderData = this.orderData;

                orderData = orderData.replace('{{orderstatus}}', allorderstatus.name);
                orderData = orderData.replace('{{value}}', allorderstatus.order_status_id);

                allorderStatus += orderData
            }
            orderwrapper = orderwrapper.replace('{{compliedTemplate}}',allorderStatus)
            return orderwrapper;
        }
}

var orderDetailsTemplate = {
      allordersDetails: '<div class="row">'
                        +    '<div class="col-md-6">'
                        +        '<div class="box border_top_col">'
                        +            '<div class="box-body row_pad">'
                        +            '<table class="row_wide">'
                        +                '<thead>'
                        +               '<tr>'
                        +                '<th class="row_pad1"><i class="fa fa-shopping-cart fa-lg btn"></i><strong style="font-size: 18px;"><span class="pad_left1">Order Details</span></strong></th>'
                        +                '</tr>'
                        +                '</thead>'
                        +                '<tbody id="orderDetails">'
                        +                '<tr>'
                        +                '<td class="border_details"><i class="fa fa-map-marker btn btn-primary"></i><span class="pad_left">{{tracking}}</span>'
                        +                '</td>'
                        +                '</tr>'
                        +                '<tr>'
                        +                '<td class="border_details"><i class="fa fa-calendar btn btn-primary"></i><span class="pad_left">{{date}}</span>'
                        +                '</td>'
                        +                '</tr>'
                        +                '<tr>'
                        +                '<td class="row_pad1"><i class="fa fa-credit-card btn btn-primary"></i><span class="pad_left">{{paymode}}</span></td>'
                        +                '</tr>'
                        +                '</tbody>'
                        +            '</table>'
                        +            '</div>'
                        +        '</div>'
                        +    '</div>'
                        +    '<div class="col-md-6">'
                        +        '<div class="box border_top_col">'
                        +            '<div class="box-body row_pad">'
                        +            '<table class="row_wide">'
                        +                '<thead>'
                        +                '<tr>'
                        +                '<th class="row_pad1"><i class="fa fa-user fa-lg btn"></i><strong style="font-size: 18px;"><span class="pad_left1">Customer Details</span></strong></th>'
                        +                '</tr>'
                        +                '</thead>'
                        +                '<tbody>'
                        +                '<tr>'
                        +                '<td class="border_details"><i class="fa fa-user btn btn-primary"></i><span class="pad_left">{{custName}}</span>'
                        +                '</td>'
                        +                '</tr>'
                        +                '<tr>'
                        +                '<td class="border_details"><i class="fa fa-envelope btn btn-primary"></i><span class="pad_left">{{custMail}}</span>'
                        +                '</td>'
                        +                '</tr>'
                        +                '<tr>'
                        +                '<td class="row_pad1"><i class="fa fa-phone btn btn-primary"></i><span class="pad_left">{{custNum}}</span></td>'
                        +                '</tr>'
                        +               ' </tbody>'
                        +            '</table>'
                        +            '</div>'
                        +        '</div>'
                        +    '</div>'
                        +'</div>'
                        +'<div class="row">'
                        +    '<div class="col-md-12">'
                        +        '<div class="box border_top_col">'
                        +            '<div class="box-header border_edit">'
                        +            '<h3 class="box-title"><i class="fa fa-info-circle"></i><strong class="font_edit1"> Order</strong></h3>'
                        +            '</div>'
                        +            '<div class="box-body">'
                        +            '<table class="table table-bordered">'
                        +                '<tr>'
                        +                '<th class="bor_edit">Payment Address</th>'
                        +                '</tr>'
                        +                '<tr>'
                        +                '<td class="bor_edit">'
                        +                    '<p>{{addrsName}}</p> <p>{{address1}}</p> <p>{{address2}}</p><p>{{city}}<span class="pincode">{{pincode}}</span></p><p>{{zone}}</p><p>{{country}}</p>'
                        +                '</td>'
                        +                '</tr>'
                        +            '</table>'
                        +            '</div>'
                        +            '<div class="box-body">'
                        +            '<table class="table table-bordered">'
                        +                '<tr>'
                        +                '<th class="bor_edit">Shipping Address</th>'
                        +                '</tr>'
                        +                '<tr>'
                        +                '<td class="bor_edit">'
                        +                    '<p>{{shipaddrsName}}</p><p>{{shipaddress1}}</p> <p>{{shipaddress2}}</p><p>{{shipcity}}</p><p>{{shippincode}}</p><p>{{shipzone}}</p><p>{{shipcountry}}</p>'
                        +                '</td>'
                        +                '</tr>'
                        +            '</table>'
                        +            '</div>'
                        +            '<div class="box-body">'
                        +            '<table class="table table-bordered">'
                        +                '<tr>'
                        +                '<th class="bor_edit">Product</th>'
                        +                '<th class="bor_edit">Model</th>'
                        +                '<th class="right_shift bor_edit">Unit Price</th>'
                        +                '<th class="right_shift bor_edit">Total</th>'
                        +                '</tr>'
                        +                '<tr>'
                        +                '<td class="bor_edit">'
                        +                    '<p class="prod_col">{{productName}}<br><span class="pad_l">-Size:{{sizes}}</span></p>'
                        +                '</td>'
                        +                '<td class="bor_edit"></td>'
                        +                '<td class="bor_edit" align="right">{{unitprice}}</td>'
                        +                '<td class="bor_edit" align="right">{{unitTotalprice}}</td>'
                        +                '</tr>'
                        +                '<tr>'
                        +                '<td class="bor_edit" colspan="3" align="right">Price</span></td>'
                        +                '<td class="bor_edit" align="right">{{price}}</td>'
                        +                '</tr>'
                        +                '<tr>'
                        +                '<td class="bor_edit" colspan="3" align="right">VAT</span></td>'
                        +                '<td class="bor_edit" align="right">{{vat}}</td>'
                        +                '</tr>'
                        +                '<tr>'
                        +                '<td class="bor_edit" colspan="3" align="right">Delivery Charge</span></td>'
                        +                '<td class="bor_edit" align="right">{{deliveryCharge}}</td>'
                        +                '</tr>'
                        +                '<tr>'
                        +                '<td class="bor_edit" colspan="3" align="right">Total Price</span></td>'
                        +                '<td class="bor_edit" align="right">{{totalPrice}}</td>'
                        +                '</tr>'
                        +            '</table>'
                        +            '</div>'
                        +        '</div>'
                        +    '</div>'
                        +'</div>'
                        +'<div class="row">'
                        +    '<div class="col-md-12">'
                        +        '<div class="box border_top_col">'
                        +            '<div class="box-header border_edit">'
                        +            '<h3 class="box-title"><i class="fa fa-comment-o"></i><strong> Order History</strong></h3>'
                        +            '</div>'
                        +            '<div class="box-body">'
                        +            '<table class="table table-bordered">'
                        +                '<tr>'
                        +                '<th class="border-no">History</th>'
                        +                '</tr>'
                        +            '</table>'
                        +            '</div>'
                        +            '<div class="box-body">'
                        +            '<table class="table table-bordered">'
                        +                '<tr>'
                        +                '<th class="bor_edit">Date Added</th>'
                        +                '<th class="bor_edit">Comment</th>'
                        +                '<th class="bor_edit">Status</th>'
                        +                '</tr>'
                        +               '{{history}}'    
                        +            '</table>'
                        +            '</div>'
                        +        '</div>'
                        +    '</div>'
                        +'</div>', 

    deliveryHistory: '<tr>'
        +                '<td class="bor_edit">{{dateAdded}}</td>'
        +                '<td class="bor_edit"></td>'
        +                '<td class="bor_edit">{{addedstatus}}</td>'
        +            '</tr>' ,  


    compileTemplate:function(orderDetails){
        var allorderDetails='';
        
        var allorderdetail = this.allordersDetails;

        // debugger;
        var m = moment(orderDetails.order_details.date_added);
        var date = m.utc().format('DD/MM/YYYY');
        var time = m.utc().format('HH:mm');
        allorderdetail = allorderdetail.replace('{{tracking}}', orderDetails.order_details.tracking);
        allorderdetail = allorderdetail.replace('{{date}}', (date));
        allorderdetail = allorderdetail.replace('{{paymode}}', orderDetails.order_details.payment_method);
        allorderdetail = allorderdetail.replace('{{custName}}', orderDetails.order_details.firstname);
        allorderdetail = allorderdetail.replace('{{custMail}}', orderDetails.order_details.email);
        allorderdetail = allorderdetail.replace('{{custNum}}', orderDetails.order_details.telephone);

        allorderdetail = allorderdetail.replace('{{addrsName}}', orderDetails.order_details.payment_firstname);
        allorderdetail = allorderdetail.replace('{{address1}}', orderDetails.order_details.payment_address_1);
        allorderdetail = allorderdetail.replace('{{address2}}', orderDetails.order_details.payment_address_2);
        allorderdetail = allorderdetail.replace('{{city}}', orderDetails.order_details.payment_city);
        allorderdetail = allorderdetail.replace('{{pincode}}', orderDetails.order_details.payment_pincode);
        allorderdetail = allorderdetail.replace('{{zone}}', orderDetails.order_details.payment_zone);
        allorderdetail = allorderdetail.replace('{{country}}', orderDetails.order_details.payment_country);

        allorderdetail = allorderdetail.replace('{{shipaddrsName}}', orderDetails.order_details.shipping_firstname);
        allorderdetail = allorderdetail.replace('{{shipaddress1}}', orderDetails.order_details.shipping_address_1);
        allorderdetail = allorderdetail.replace('{{shipaddress2}}', orderDetails.order_details.shipping_address_2);
        allorderdetail = allorderdetail.replace('{{shipcity}}', orderDetails.order_details.shipping_city);
        allorderdetail = allorderdetail.replace('{{shippincode}}', orderDetails.order_details.shipping_pincode);
        allorderdetail = allorderdetail.replace('{{shipzone}}', orderDetails.order_details.shipping_zone);
        allorderdetail = allorderdetail.replace('{{shipcountry}}', orderDetails.order_details.shipping_country);
        
        allorderdetail = allorderdetail.replace('{{productName}}', orderDetails.order_details.name);
        allorderdetail = allorderdetail.replace('{{sizes}}', orderDetails.order_option.value);
        allorderdetail = allorderdetail.replace('{{unitprice}}', orderDetails.order_details.price);
        allorderdetail = allorderdetail.replace('{{unitTotalprice}}', orderDetails.order_details.price);
        allorderdetail = allorderdetail.replace('{{price}}', orderDetails.order_details.price);
        allorderdetail = allorderdetail.replace('{{vat}}', orderDetails.order_totals[1].value);
        // allorderdetail = allorderdetail.replace('{{deliveryCharge}}', orderDetails.order_details);
        allorderdetail = allorderdetail.replace('{{deliveryCharge}}', 'not applied');
        allorderdetail = allorderdetail.replace('{{totalPrice}}', orderDetails.order_totals[0].value);

        var historyTemp = '';
        for(var i = 0;i<orderDetails.order_history.length;i++){
            var history = orderDetails.order_history[i];
            var temp = this.deliveryHistory;

            var m = moment(history.date_added);
            var date = m.utc().format('DD/MM/YYYY');
            var time = m.utc().format('HH:mm');

            temp = temp.replace('{{dateAdded}}', (date +'<br>'+time));
            temp = temp.replace('{{addedstatus}}', history.name);
            historyTemp += temp;
        }
        allorderdetail = allorderdetail.replace('{{history}}',historyTemp);
        // allorderdetail = allorderdetail.replace('{{addedstatus}}', orderDetails.order_history.name);

        allorderDetails += allorderdetail;
        return allorderDetails;
    }                
}


