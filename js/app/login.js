$("#vd_submit").click(function(event){
	var vd_username = $('#vd_username').val();
	var vd_password = $('#vd_password').val();
	var request = $.ajax( {
			url: "http://35.154.54.234:1337/"+"login",
			method: "POST",
			data: "username="+vd_username+"&password="+vd_password,
			dataType: "json"
		});

		request.done(function( response ) {
			// debugger;
		  console.log(response);
		  localStorage.setItem('data',JSON.stringify(response.data));
				if(response.code == 200){
					// debugger;
					//Set Cookies for Login and Auth
					Cookies.set('auth', response.data.auth_token ,{expires:7});
                    Cookies.set('storeid', response.data.store_id ,{expires:7});
                    Cookies.set('image', response.data.image ,{expires:7});

					//Cookies Set, Redirect to Dashboard now

					if(response.data.store_details == ''){
                        Cookies.set('storename', 'STYFI' ,{expires:7});
					}   else {
                        Cookies.set('storename', response.data.store_details.store_name ,{expires:7});
					}

					window.location = "/dashboard.html";

				}
				else{
                    alert( "Request failed: " + "Wrong Username or Password" );
				}
		});

		request.fail(function( jqXHR, textStatus ) {
		  alert( "Request failed: " + "Wrong Username or Password" );
		});
	});

$(document).ready(function() {
	var auth = Cookies.get('auth');
	var storeid = Cookies.get('storeid');
	var image = Cookies.get('image');

	if(auth != undefined &&  auth.length >0 && storeid >=0){
		window.location="/dashboard.html";
	}

})